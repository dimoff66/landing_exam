<?php

namespace App\Extenders;

class References {
    
    const SUBPATHS  = array(
        'storage' => array(
            'settings' => '/settings.json',
            'splitTestData' => '/SplitTest.json'
        )
    );
    
    const DB_TABLES = array(
        
        'landingVisits' => array(
            'tableName' => 'landing_visits',
            'fieldNames' => array(
                'id' => 'id',
                'ipAddress' => 'ip_address',
                'visitDate' => 'visit_date',
                'sourceReference' => 'source_ref',
                'splitTestId' => 'split_id',
                
                'utmSource' => 'utm_source',
                'utmCampaign' => 'utm_campaign',
                'utmContent' => 'utm_content',
                'utmTerm' => 'utm_term'
            )
        ),
        
        'consultRequests' => array(
            'tableName' => 'consult_requests',
            'fieldNames' => array(
                'visitId' => 'visit_id',
                'potentialCustomerName' => 'p_customer_name',
                'potentialCustomerEmail' => 'p_customer_email',
                'clickedButtonId' => 'clicked_button_id',
                'requestDate' => 'request_date'
            )
        )
    );
    
    const GET_PARAMETERS = array(
        'utmSource' => 'utm_source',
        'utmCampaign' => 'utm_campaign',
        'utmContent' => 'utm_content',
        'utmTerm' => 'utm_term'
    );
    
    const SESSION_VARIABLES = array(
        'visitId' => 'visitId',
        'feedbackSent' => 'feedbackSent',
        'thanksPageVisited' => 'thanksPageVisited'
    );
    
    const OPEN_PAGE_NAMES_IN_DB = array(
        'top' => 'top',
        'noJsRequestForm' => 'noRequestJsForm',
        'thanks' => 'thanks'
    );
    
    const OPEN_VIEWS_FOLDER = 'open';
    const OPEN_VIEWS = array(
        
        'top' => self::OPEN_VIEWS_FOLDER.'.'.'top',
        'noJsRequestForm' => self::OPEN_VIEWS_FOLDER.'.'.'nojs_request',
        'thanks' => self::OPEN_VIEWS_FOLDER.'.'.'thanks'
    );
    
    const AUTH_VIEWS_FOLDER = 'auth';
    const AUTH_VIEWS = array(
        'login' => self::AUTH_VIEWS_FOLDER.'.'.'login',
    );
    
    const EMAIL_VIEWS = array(
        'consultRequestNotification' => 'emails'.'.'.'consultRequestNotification'
    );
    
    const OPEN_ROUTES = array(
       
        'top' => 'top',
        'noJsRequest' => 'noJsRequest',
        'submitConsultationRequest' => 'submitConsultationRequest',
        'thanksForConsultationRequest' => 'thanksForConsultationRequest'
    );
    
    const FORMS = array (
        
        'feedbackForm' => array(
            'fieldNames' => array(
                'name' => 'name',
                'email' => 'email',
                'buttonId' => 'buttonId'
            )
        )
    );
    
    const ERROR_MESSAGES = array (
        'feedbackForm' => array(
            'name.required' => 'Вы не указали Ваше имя. Пожалуйста, укажите имя (фамилию и отчество не обязательно).',
            'name.max' => 'Вы указали слишком длинное имя: позволено не более 255 символов.',
            'email.required' => 'Вы не указали адрес электронной почты. Пожалуйста, укажите адрес электронной почты.',
            'email.email' => 'Вы ввели невозможный адрес электронной почты. Пожалуйста, проверьте введённый email-адрес.',
            'twoRequestsImpossible' => 'Отправка двух и более заявок подряд невозможна. Если Вы хотите создать '
                    . 'два или более лендингов, скажите об этом менеджеру, когда он с Вами свяжется.' 
        )
    );
    
    const SETTINGS = array (
       'FeedbackRequest' => array(
           'FirstReplyMethods' => array(
                'FullAutoReply',
                'FullAutoReplyAfterWaiting',
                'SemiAutoReply',
                'FullManualReply'
           )
       )
    );
}