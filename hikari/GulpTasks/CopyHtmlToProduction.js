'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        return gulp.src(options.HPath.htmlFilesSelectionForCopyingToProduction)
            .pipe(gulp.dest( file => {

                let fileBase = file.base;

                if (fileBase.indexOf(options.HPath.folderNames.development.devBuild.open.root) !== -1) {
                    return options.HPath.htmlOutputPaths.productionBuild.open;
                }
                else if (fileBase.indexOf(options.HPath.folderNames.development.devBuild.admin.root) !== -1) {
                    return options.HPath.htmlOutputPaths.productionBuild.admin;
                }
                else {
                    throw new Error('Copy HTML to Production 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                }
            }));
    };
};