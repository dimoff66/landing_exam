/* global __dirname */

'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')(),
            gulplog = require('gulplog'),
    
            glob = require('glob'),
            path = require('path'),
            notifier = require('node-notifier'),
            
            webpack = require('webpack');


module.exports = options => {
    
    return callback=> {
        
        let WebpackEntryPoints = getWebpackEntryPoints(options.HPath);
        
        if (Object.keys(WebpackEntryPoints).length === 0 && WebpackEntryPoints.constructor === Object) {
            console.log('Webpackエントリーポイント発見されません、Webpack課題中断。');
            return callback();
        }
        
        const projectRootDirectory = require('path').resolve(__dirname,'..','..');
        const output_path = options.isDevelopment ? (projectRootDirectory + '/development/2_devBuild') : (projectRootDirectory + '/public');
    
        let WebpackOptions = {
        
            entry: WebpackEntryPoints,

            output: {
                path: output_path,
                filename: '[name].js'
            },

            watch: options.isDevelopment,

            devtool: options.isDevelopment ? 'cheap-module-inline-source-map': false,

            module: {
                rules: [{
                    test: /\.js$/,
                    use: [{
                        loader: 'babel-loader',
                            options: {
                                presets: [
                                    ['env', {'modules': false}]
                                ]
                            }
                    }],
                    exclude: /node_modules/
                }]
            },

            plugins: [
                new webpack.NoEmitOnErrorsPlugin(),
                new webpack.optimize.UglifyJsPlugin({
                    output: {
                        beautify: options.isDevelopment ? true: false
                    }
                }),
                new webpack.ProvidePlugin({
                    jQuery: 'jquery',
                    $: 'jquery'
                }),
                new webpack.DefinePlugin({
                    IS_DEVELOPMENT: options.isDevelopment
                })
            ],
            
            resolve: {
                alias: {
                    FrameworkRoot: path.resolve(__dirname, './../'),
                    JsExtenders: path.resolve(__dirname, './../JsExtenders')
                }
            }
        };
    
        webpack(WebpackOptions, (error, statistics) => {
        
            // ハードエラーでが無ければ、統計でソフトエラー確認
            if (!error) { 
                error = statistics.toJson().errors[0];
            }

            if (error) {

                notifier.notify({
                    title: 'webpack',
                    message: error
                });

                gulplog.error(error);

            } else {

                gulplog.info(statistics.toString({
                    colors: true
                }));
            }


            if (!WebpackOptions.watch && error) {
                // 納品版の場合のみ
                callback(error);
            }
            else {
                callback();
            }
        });
    };
};


function getWebpackEntryPoints(HPath){
    
    let webpackEntryPoints = {};
    
    let entryPointsGlobObject = glob.sync(HPath.stringifiedEs6SourceFilesSelectionGlobPattern);
    
    entryPointsGlobObject.forEach((value, index, array) => {

        let filePathWithoutFileExtention = value.replace(/.js$/, '');
        let sourceEntryPoint = './' + filePathWithoutFileExtention;

        // 例： 1_public/js/test
        let outputBundlePath;

        let fileNameStartPosition = filePathWithoutFileExtention.lastIndexOf('/') + 1;

        if (value.indexOf(HPath.folderNames.development.source.open.root) !== -1) {
            outputBundlePath = HPath.folderNames.development.devBuild.open.root + '/' 
                    + HPath.folderNames.development.devBuild.open.js + '/' 
                    + filePathWithoutFileExtention.substring(fileNameStartPosition);
        }
        else if (value.indexOf(HPath.folderNames.development.source.admin.root) !== -1) {
            outputBundlePath = HPath.folderNames.development.devBuild.admin.root + '/' 
                    + HPath.folderNames.development.devBuild.admin.js + '/' 
                    + filePathWithoutFileExtention.substring(fileNameStartPosition);
        }

        webpackEntryPoints[outputBundlePath] = sourceEntryPoint;

    });
    
    return(webpackEntryPoints);
    
};