'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        return gulp.src(options.HPath.emailPugSourceFilesSelection)
        
            .pipe(plugins.plumber({
                errorHandler: plugins.notify.onError( error => {
                    return {
                        title: 'Prepare Emails Initial HTML:',
                        message: error.message
                    };
                })
            }))
            
            .pipe(plugins.pug())
    
            .pipe(gulp.dest( file => {

                let fileBase = file.base;

                if (fileBase.indexOf(options.HPath.folderNames.development.devBuild.admin.root) !== -1) {
                    return options.HPath.emailsHtmlFilesOutput;
                }
                else {
                    throw new Error('Prepare Emails Initial HTML 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                }
            }));
    };
};