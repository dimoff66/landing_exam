'use strict';

const   gulp = require('gulp'),
            gulpIf = require('gulp-if'),
            sourcemaps = require('gulp-sourcemaps'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        let sassFilesSelection;
        
        if (options.isDevelopment) {
            sassFilesSelection = options.HPath.sassSourceFilesSelection.development;
        }
        else {
            sassFilesSelection = options.HPath.sassSourceFilesSelection.production;
        }
        
        
        let PostCssPlugins = [
                require('autoprefixer')({ browsers: ['>= 1%', 'last 5 major versions', 'ie >= 6']}),
                require('cssnano-util-raw-cache')(),
                require('postcss-calc')(),
                require('postcss-colormin')(),
                require('postcss-discard-duplicates')(),
                require('postcss-discard-overridden')(),
                require('postcss-merge-longhand')(),
                require('postcss-merge-rules')(),
                require('postcss-minify-font-values')(),
                require('postcss-minify-gradients')(),
                require('postcss-normalize-display-values')(),
                require('postcss-normalize-positions')(),
                require('postcss-normalize-string')(),
                require('postcss-normalize-timing-functions')(),
                require('postcss-normalize-unicode')(),
                require('postcss-normalize-url')(),
                require('postcss-ordered-values')(),
                require('postcss-reduce-initial')(),
                require('postcss-reduce-transforms')(),
                require('postcss-svgo')(),
                require('postcss-unique-selectors')(),
                !options.isDevelopment ? require('postcss-normalize-whitespace')() : false,
                !options.isDevelopment ? require('postcss-discard-comments')() : false
        ].filter(Boolean);
    
    
        return gulp.src(sassFilesSelection)
            .pipe(plugins.plumber({
                errorHandler: plugins.notify.onError( error => {
                    return {
                        title: 'styles',
                        message: error.message
                    };
                })
            }))
            .pipe(gulpIf(options.isDevelopment, sourcemaps.init()))
            .pipe(plugins.sass())
            .pipe(gulpIf(options.isDevelopment, sourcemaps.write()))
            .pipe(plugins.postcss(PostCssPlugins))
            //.pipe(plugins.if(!options.isDevelopment, rename({suffix: '.min'})))
            .pipe(gulp.dest( file => {

                let fileBase = file.base;
        
                if (fileBase.indexOf(options.HPath.folderNames.development.source.open.root) !== -1) {
                    if (options.isDevelopment) {
                        return options.HPath.cssOutputPaths.developmentBuild.open;
                    }
                    return options.HPath.cssOutputPaths.productionBuild.open;
                }
                else if (fileBase.indexOf(options.HPath.folderNames.development.source.admin.root) !== -1) {
                    if (options.isDevelopment) {
                        return options.HPath.cssOutputPaths.developmentBuild.admin;
                    }
                    return options.HPath.cssOutputPaths.productionBuild.admin;
                }
                // 納品版の時は此方には来ない様に設定しなければならない。
                else if (fileBase.indexOf(options.HPath.folderNames.development.source.development.root) !== -1) {
                    return options.HPath.cssOutputPaths.developmentBuild.development;
                }
                else {
                    throw new Error('Styles 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                }
            }));
    };
};