'use strict';

const   gulp = require('gulp'),
            gulpIf = require('gulp-if'),
            fs = require('fs'),
            plugins = require('gulp-load-plugins')();


module.exports = options => {
    
    return () => {
        return gulp.src(options.HPath.fontsSourceFilesSelection)
                .pipe(plugins.debug({title: 'fonts'}))
                .pipe(gulp.dest( file => {

                    let fileBase = file.base;
            
                    options.isDevelopment = !options.isDevelopment;
        
                    if (fileBase.indexOf(options.HPath.folderNames.development.source.common.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.fontsOutputPaths.developmentBuild.common;
                        }
                        return options.HPath.fontsOutputPaths.productionBuild.common;
                    }
                    if (fileBase.indexOf(options.HPath.folderNames.development.source.open.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.fontsOutputPaths.developmentBuild.open;
                        }
                        return options.HPath.fontsOutputPaths.productionBuild.open;
                    }
                    else if (fileBase.indexOf(options.HPath.folderNames.development.source.admin.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.fontsOutputPaths.developmentBuild.admin;
                        }
                        return options.HPath.fontsOutputPaths.productionBuild.admin;
                    }
                    // 納品版の時は此方には来ない様に設定しなければならない。
                    else if (fileBase.indexOf(options.HPath.folderNames.development.source.development.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.fontsOutputPaths.developmentBuild.development;
                        }
                        return options.HPath.fontsOutputPaths.productionBuild.development;
                    }
                    else {
                        throw new Error('fonts 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                    }
            }));
    };
};