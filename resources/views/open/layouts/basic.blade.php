<!DOCTYPE html>
<html lang="ru">
    <head>

        <title>myPhone новый хороший смартфон , не хуже чем телефон apple айфон и дешевле</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="Author" content="Reutov Dmitry">
        <meta name="description" content="Приобретая новую модель телефона MyPhone вы становитесь обладетелем престижного смартфона, сопоставимого по качеству с моделями телефонов Айфон.">
        <meta name="Keywords" content="Смартфон, мобильный телефон, айфон">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('common/img/favicon.ico') }}">

        @yield('CssLinks')
        
        <?php /*
        * Обычно в head мы скрипты не подключаем; только если это какой-то частный 
        * случай типа кода от Google Analytics.
        */?>
        @yield('EndHeadJS')
    </head>

    <body>
        
        {{-- Шапка с меню --}}
        @include('open/widgets/RegularHeaderWithMenu')
        
        {{-- Основной контент (задаётся в унаследованных шаблонах ) --}}
        @yield('Content')
        
        {{-- Футер --}}
        @include('open/widgets/RegularFooter')
        
        {{-- Скрытые элементы типа модальных окон --}}
        @yield('HiddenElements')
        
        {{-- Подключение скриптов (должен быть только один файл) --}}
        @yield('EndBodyJS')
    </body>
</html>