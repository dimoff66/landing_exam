<header>
    <div class="headerBlock Centerer-MinMaxGridWidth">
        <a class="logo-link" href="#">
            <img class="img-logo" src="img/mhgsmall.png" title="Прочный союз качества и стиля. На вашей свадьбе подруги умрут от зависти" alt="Прочный союз качества и стиля. На вашей свадьбе подруги умрут от зависти">
        </a>
        <nav class="head-menu">
            <ul class="menulist">
                <li class="menuitem"><a class="a" href="#">О компании</a>
                </li>
                <li class="menuitem"><a class="a" href="#">Товары</a>
                </li>
                <li class="menuitem"><a class="a" href="#">Услуги</a>
                </li>
                <li class="menuitem"><a class="a" href="#">Схема проезда</a>
                </li>
                <li class="menuitem accent" onclick="modalDlg.open(&quot;.FeedbackForm-RootElement .container&quot;, &quot;opened&quot;, &quot;#feedbackModal.modalBackground&quot;);"><a class="a" href="#">Связаться</a>
                </li>
                <li class="menuitem drop-menu" onclick="modalDlg.open(&quot;.sidebar&quot;, &quot;opened&quot;, &quot;#sidebarModal.modalBackground&quot;);">
                    <div class="a">
                        <div class="hamburger">&#9776;</div>
                        <div class="menulabel">МЕНЮ</div>
                        <noscript>
                            <ul class="dropdown">
                                <li class="submenuitem"><a href="#">О компании</a>
                                </li>
                                <li class="submenuitem"><a href="#">Товары</a>
                                </li>
                                <li class="submenuitem"><a href="#">Услуги</a>
                                </li>
                                <li class="submenuitem"><a href="#">Схема проезда</a>
                                </li>
                            </ul>
                        </noscript>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</header>