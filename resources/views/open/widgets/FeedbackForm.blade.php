<div class="modalBackground" id="feedbackModal">
    <div class="FeedbackForm-RootElement">
        <div class="container">
            <div class="companyContactsBlock">
                <h3 class="title">Адреса и телефоны</h3>
                <ul class="contactsList">
                    <li class="type_phone">+7 495 123-45-67</li>
                    <li class="type_email">mail@company.ru</li>
                    <li class="type_address">Москва, ул. Академика Порохова, дом 20, Дом 4, подъезд 145, звонить</li>
                </ul>
            </div>
            <div class="feedbackBlock">
                <form id="feedback" action="#" method="post">
                    <h3 class="title">Форма заявки</h3>
                    <div class="fields">
                        <div class="fieldBlock">
                            <div class="label">Ваше имя</div>
                            <div class="field">
                                <input class="type_name" placeholder="Пример: Петров Сергей" name="name" type="text" pattern="[A-Za-zА-Яа-яЁё]+" required>
                            </div>
                        </div>
                        <div class="errorBlock type_name">
                            <div class="error">Вы не ввели ваше имя, пожалуйста введите ваше имя</div>
                            <div class="error">Вы не ввели ни одной буквы, пожалуйста введите ваше имя</div>
                        </div>
                        <div class="fieldBlock">
                            <div class="label">Ваша электронная почта</div>
                            <div class="field">
                                <input class="type_email" placeholder="Пример: petrov@mail.ru" name="email" type="text" pattern=".+@.+\..+" required>
                            </div>
                        </div>
                        <div class="errorBlock type_email">
                            <div class="error">Вы не ввели адрес электронной почты. Пожалуйста введите адрес электронной почты</div>
                            <div class="error">Вы ввели невозможный адрес электронной почты, пожалуйста проверьте введенный адрес</div>
                        </div>
                        <div class="fieldBlock">
                            <div class="label">Ваш телефон</div>
                            <div class="field">
                                <input class="type_phone" placeholder="Пример: +7 495 234-56-78" name="phone" type="tel" pattern="[0-9]{2,}" required>
                            </div>
                        </div>
                        <div class="errorBlock type_phone">
                            <div class="error">Вы не ввели номер телефона. Пожалуйста введите номер телефона.</div>
                            <div class="error">Вы ввели невозможный номер телефона. Пожалуйста проверьте ваш номер телефона</div>
                        </div>
                    </div>
                    <div class="message">
                        <p>Сообщение:</p>
                        <textarea></textarea>
                    </div>
                    <div class="settings">
                        <div class="column1">
                            <div class="checkboxes">
                                <label>
                                    <input type="checkbox" name="subscribe1"><span class="checkbox"></span><span class="label">Подписаться&nbsp;на&nbsp;рассылку&nbsp;1</span>
                                </label>
                                <label>
                                    <input type="checkbox" name="subscribe2"><span class="checkbox"></span><span class="label">Подписаться&nbsp;на&nbsp;рассылку&nbsp;2</span>
                                </label>
                            </div>
                            <div class="connectMethodBlock">
                                <p>Предпочтительный способ связи</p>
                                <label class="radio">
                                    <input type="radio" id="method1" name="connect-method" value="email"><span class="option"><span></span></span><span class="label">Электронная почта</span>
                                </label>
                                <label class="radio">
                                    <input type="radio" id="method2" name="connect-method" value="phone"><span class="option"><span></span></span><span class="label">Телефон</span>
                                </label>
                            </div>
                        </div>
                        <div class="column2">
                            <div class="switcherContainer">
                                <label>
                                    <input type="checkbox" name="setting1"><span class="circle color1"></span><span class="lbl color1" onclick="event.preventDefault();"> Настройка&nbsp;1</span>
                                </label>
                            </div>
                            <div class="switcherContainer">
                                <label>
                                    <input type="checkbox" name="setting2"><span class="circle color2"></span><span class="lbl color2" onclick="event.preventDefault();">Настройка&nbsp;2</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="errorInfo">Остались не введенные или неправильно введенные поля. Пожалуйста, проверьте данные!</div>
                    <div class="submitBlock">
                        <input id="submitButton" type="submit" value="Отправить" onclick="errors.check(event);">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>