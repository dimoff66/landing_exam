<div class="modalBackground" id="sidebarModal">
    <div class="sidebar">
        <div class="top"><span class="menuTitle">МЕНЮ</span><span class="rightTopButton"><span class="closeDlgButton"> <a href="#">X</a></span><span class="closeButtonTitle">Закрыть</span></span>
        </div>
        <div class="menuItems">
            <div class="menu-item-row">
                <div class="menu-item-icon"></div>
                <div class="menu-item-text"><a href="#">О компании</a>
                </div>
            </div>
            <div class="menu-item-row">
                <div class="menu-item-icon"></div>
                <div class="menu-item-text"><a href="#">Товары</a>
                </div>
            </div>
            <div class="menu-item-row">
                <div class="menu-item-icon"></div>
                <div class="menu-item-text"><a href="#">Услуги</a>
                </div>
            </div>
            <div class="menu-item-row">
                <div class="menu-item-icon"></div>
                <div class="menu-item-text"><a href="#">Схема проезда</a>
                </div>
            </div>
            <div class="menu-item-row">
                <div class="menu-item-icon"></div>
                <div class="menu-item-text"><a href="#">Связаться</a>
                </div>
            </div>
        </div>
    </div>
</div>