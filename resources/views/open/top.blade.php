@extends('open/layouts/basic')

@section('CssLinks')
    <link href="{{ asset('open/css/01_Top.css') }}" rel="stylesheet">
@endsection


@section('Content')

    <?php /*
    * Это сообщения, которые генерируются сервером при отправке невалидных данных. 
    * В идеале, засчёт валидации данных средствами HTML и JavaScript эти сообщения никогда 
    * не должны отобразиться, тем менее проверка данных на стороне сервера необходима 
    * (опять же, она уже реализована, Вам же осталось только сверстать сообщения об ошибке).
    */?>

    @if(count($errors) > 0)
        <div class="FeedbackForm-ServerErrors-RootWrapper">
            <div class="FeedbackForm-ServerErrors-Centerer">
                <div class="FeedbackForm-ServerErrors-Title">
                    При отправке формы заявки обнаружены следующие ошибки:
                </div>
                <ul class="FeedbackForm-ServerErrors-ErrorsList">
                    @foreach($errors -> all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <?php /*
    * Вставьте сюда основное содержимое страницы
    */?>
    
    <!-- include('open/01_Top/01_Slider')-->
        <div class="sliderBlock Centerer-MinMaxGridWidth">
            <div class="sliderImagesContainer">
                <img class="slider selected" src="img/sliders/slider1.jpg" alt="slider show">
                <img class="slider" src="img/sliders/slider2.jpg" alt="slider show">
                <img class="slider" src="img/sliders/slider3.jpg" alt="slider show">
                <img class="slider" src="img/sliders/slider4.png" alt="slider show">
            </div>
            <div class="miniPictures">
                <div class="textBlock">
                    <p data-sentence0="Не то, чтобы он был слишком опрятным человеком, но определенная бесполезность и бессмысленность взгляда выдавала в нем человека крайне незаурядного, что впрочем не сильно ценится в наше время" data-sentence1="Случайный взгляд на луну способен породить в человеке огромное количество сомнений, которые разрешить не под силу в одиночку, ведь снегопад вечности заметает любые следы мысли"
                    data-sentence2="И когда они ткнули в меня пальцем и сказали, что я виновник всех их бед, я скромно потупился и ответил, что в сущности я программист от Бога, а когда мои программы не работают - стало быть Бог наказывает грешников"
                    data-sentence3="Сапоги бывают не на ту ногу лишь в тех редких случаях, когда у человека отсутствует та нога, лишь две эти, да и то без костей">Не то, чтобы он был слишком опрятным человеком, но определенная бесполезность и бессмысленность взгляда выдавала в нем человека крайне незаурядного, что впрочем не сильно ценится в наше время</p>
                </div>
                <div class="detailsLink"><a href="#">Подробнее</a>
                </div>
                <div class="container">
                    <img class="minipicture selected" src="img/sliders/small/slider1.jpg" alt="slider show" onclick="slider.onSelectMiniPicture(this, 0);">
                    <img class="minipicture" src="img/sliders/small/slider2.jpg" alt="slider show" onclick="slider.onSelectMiniPicture(this, 0);">
                    <img class="minipicture" src="img/sliders/small/slider3.jpg" alt="slider show" onclick="slider.onSelectMiniPicture(this, 0);">
                    <img class="minipicture" src="img/sliders/small/slider4.png" alt="slider show" onclick="slider.onSelectMiniPicture(this, 0);">
                </div>
            </div>
        </div>
        <!-- include('open/01_Top/02_CustomGrid')-->
        <div class="customGridBlock Centerer-MinMaxGridWidth">
            <div class="customGridTitle">
                <h3 class="title1">Жесть... Как я реализую это на Bootstrap ?!</h3>
                <h5 class="title2">Bootstrap вгоняет ваше мышление в определенные рамки</h5>
            </div>
            <div class="customGridContainer">
                <div class="column1">
                    <div class="columnTitle">Жирафы честно в цель шагают, да щук объять за память елкой...</div>
                    <div class="imgCenter">
                        <img src="img/preview_pillow.png" alt="bootstrap sucks">
                    </div>
                    <div class="columnText">Пересекая двойную сплошную между возможным и невозможным Петька почувствовал сзади могильный холодок, словно бы сама смерть решила сыграть с ним в салки...
                    </div>
                </div>
                <div class="column2">
                    <div class="columnTitle">В чащах юга жил-был цитрус...но фальшивый, как телега
                    </div>
                    <ol class="orderedList">
                        <li><span class="columnText">Притяжение земли не сразу настигло канатоходца
Некоторое время он еще перебирал в воздухе беспомощно
ногами, словно надеясь избежать рокового падения
</span>
                        </li>
                        <li><span class="columnText">Жаль что нет с собой трезубца, - посетовал Ильич
глядя на убегающих прочь скакунов, - ведь где их найдешь
теперь нерадивых. Пашня то близится, а сеятелей кот наплакал.
</span>
                        </li>
                        <li><span class="columnText">Изменяя каждую пришедшую в голову мысль, Иван поскользнулся внутрь 
пульсирующего пространства, которое казалось неким странным отражением
глубины его потрепанного годами бестоковой жизни сознания.
</span>
                        </li>
                        <li><span class="columnText">Последняя надежда была лишь на то, что полка в конце концов развалится
и не нужно будет больше читать всю эту груду безнравственной литературы.
</span>
                        </li>
                        <li><span class="columnText">Эка невидаль эти ваши смартфоны... А фот зайца на лесной тропе ваш
смартфон сможет выследить? Сможет? Нет, скажи мне - сможет? То-то же... Гаврила
довольно затянулся и посмотрел туда, где река превращалась в тоненький ручеек



</span>
                        </li>
                    </ol>
                </div>
                <div class="column3">
                    <div class="columnTitle">Расчешись...Объявляю: туфли у камина</div>
                    <ul class="unorderedList">
                        <li>
                            <div class="ulLabelContainer">
                                <div class="ulLabel1"></div>
                                <div class="ulLabel2"></div>
                            </div><span class="columnText">Почти освоившись с некоторыми нотами, следует переходить
к следующему этапу беседы - выдувать ля-минор языком, едва
касаясь его кончиком солоноватых губ
                 </span>
                        </li>
                        <li>
                            <div class="ulLabelContainer">
                                <div class="ulLabel1"></div>
                                <div class="ulLabel2"></div>
                            </div><span class="columnText">Грибы всегда были съедобными, и их ели и они ели, борьба за выживание
происходила по всем фронтам, никогда нельзя быть уверенным в своем превосходстве
                          </span>
                        </li>
                        <li>
                            <div class="ulLabelContainer">
                                <div class="ulLabel1"></div>
                                <div class="ulLabel2"></div>
                                <!-- include('open/01_Top/03_VerticalCentering')-->
                            </div><span class="columnText">Я бы не стал уж слишком подробно останавливаться на этом моменте, - печально сказал Ломоносов,
понимание того, что наука не всесильна и лишь едва касается той самой сокровенной реальности,
которую с таким самозабвенным упоением пытается исследовать, неизбежно приведет к упадническим настроениям
в ученой среде...</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="verticalCenteringBlock Centerer-MinMaxGridWidth">
            <p class="blockTitle">Задача вертикального центрирования</p>
            <div class="imagesGrid">
                <div class="column">
                    <div class="imageFrame">
                        <img src="img/vertical_2.png" alt="vertical_2.png">
                    </div>
                </div>
                <div class="column">
                    <div class="imageFrame">
                        <img src="img/vertical_3.jpg" alt="vertical_3.jpg">
                    </div>
                </div>
                <div class="column">
                    <div class="imageFrame">
                        <img src="img/vertical_1.jpg" alt="vertical_1.jpg">
                    </div>
                </div>
            </div>
        </div>
        <!-- include('open/01_Top/04_CardsView')-->
        <div class="CardViewBlock Centerer-MinMaxGridWidth">
            <div class="cardColumn column1">
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic1.jpg" alt="Luke Sutton">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Luke Sutton</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">luke_sutton@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic2.jpg" alt="Bruce Gordon">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Bruce Gordon</span>
                                <div class="circle color-2"></div>
                            </div>
                            <div class="personMailAddress">bruce_gordon@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic3.jpg" alt="Christina Nickols">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Christina Nickols</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">christina_nickols@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic1.jpg" alt="Luke Sutton">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Luke Sutton</span>
                                <div class="circle color-3"></div>
                            </div>
                            <div class="personMailAddress">luke_sutton@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="cardColumn column2">
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic2.jpg" alt="Bruce Gordon">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Bruce Gordon</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">bruce_gordon@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic3.jpg" alt="Christina Nickols">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Christina Nickols</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">christina_nickols@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic1.jpg" alt="Luke Sutton">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Luke Sutton</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">luke_sutton@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="card">
                    <tr class="row1">
                        <td class="fotocontainer" rowspan="2">
                            <img class="foto" src="img/team/team_pic2.jpg" alt="Bruce Gordon">
                        </td>
                        <td class="title">
                            <div class="personName"> <span>Bruce Gordon</span>
                                <div class="circle color-1"></div>
                            </div>
                            <div class="personMailAddress">bruce_gordon@company.com</div>
                        </td>
                        <td class="menuContainer">
                            <div class="menuIcon">
                                <div class="menu">
                                    <ul>
                                        <li class="edit" onclick="cards.edit(this)">Редактировать</li>
                                        <li class="delete" onclick="cards.delete(this)">Удалить</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="row2">
                        <td class="info"><span>The quick brown fox jumps over a lazy dog. MTV aux quiz prog. Junk MTV quiz</span>
                            <textarea onblur="cards.finishEditing(this)"></textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
      
@endsection

@section('HiddenElements')

    <?php /*
    * Сюда можно подключить скрытые абсолютно позиционируемые элементы, такие как модальные окна, 
     * в том числе окно с формой обратной связи.
    */?>
	
	@include('open/widgets/SideBarMenu')
    @include('open/widgets/FeedbackForm')
@endsection

@section('EndBodyJS')
    <script src="{{ asset('open/js/Top.js') }}"></script>
@endsection