<?php

use Illuminate\Database\Seeder;
use App\Extenders\References as REF;

class PagesTableSeeder extends Seeder {
    
        public function run() {
        
        DB::table(REF::DB_TABLES['pages']['tableName'])->insert([
            [
                REF::DB_TABLES['pages']['fieldNames']['pagename'] => REF::OPEN_PAGE_NAMES_IN_DB['top'],
                REF::DB_TABLES['pages']['fieldNames']['title'] => '',
                REF::DB_TABLES['pages']['fieldNames']['metadesc'] => '',
                REF::DB_TABLES['pages']['fieldNames']['keywords'] => ''
            ],
            [
                REF::DB_TABLES['pages']['fieldNames']['pagename'] => REF::OPEN_PAGE_NAMES_IN_DB['noJsRequestForm'],
                REF::DB_TABLES['pages']['fieldNames']['title'] => '',
                REF::DB_TABLES['pages']['fieldNames']['metadesc'] => '',
                REF::DB_TABLES['pages']['fieldNames']['keywords'] => ''
            ],
            [
                REF::DB_TABLES['pages']['fieldNames']['pagename'] => REF::OPEN_PAGE_NAMES_IN_DB['thanks'],
                REF::DB_TABLES['pages']['fieldNames']['title'] => '',
                REF::DB_TABLES['pages']['fieldNames']['metadesc'] => '',
                REF::DB_TABLES['pages']['fieldNames']['keywords'] => ''
            ]
        ]);
    }
}