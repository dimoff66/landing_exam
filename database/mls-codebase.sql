-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 2018 年 2 朁E27 日 14:08
-- サーバのバージョン： 5.5.53
-- PHP Version: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mls-codebase`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `consult_requests`
--

CREATE TABLE `consult_requests` (
  `visit_id` int(11) NOT NULL,
  `p_customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_customer_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clicked_button_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `landing_visits`
--

CREATE TABLE `landing_visits` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_date` timestamp NULL DEFAULT NULL,
  `source_ref` text COLLATE utf8mb4_unicode_ci,
  `split_id` int(11) DEFAULT NULL,
  `utm_source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utm_campaign` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utm_content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utm_term` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `landing_visits`
--

INSERT INTO `landing_visits` (`id`, `ip_address`, `visit_date`, `source_ref`, `split_id`, `utm_source`, `utm_campaign`, `utm_content`, `utm_term`) VALUES
(1, '127.0.0.1', '2018-02-27 05:07:47', NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2018_02_08_014911_create_consultation_requests_table', 1),
(9, '2018_02_08_014931_create_landing_visits_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consult_requests`
--
ALTER TABLE `consult_requests`
  ADD UNIQUE KEY `consult_requests_visit_id_unique` (`visit_id`);

--
-- Indexes for table `landing_visits`
--
ALTER TABLE `landing_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `landing_visits`
--
ALTER TABLE `landing_visits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
