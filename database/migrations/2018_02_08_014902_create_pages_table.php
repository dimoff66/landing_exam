<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Extenders\References as REF;

class CreatePagesTable extends Migration {
    
    public function up() {
        
        Schema::create(REF::DB_TABLES['pages']['tableName'], function (Blueprint $table) {
            
            $table->increments(REF::DB_TABLES['pages']['fieldNames']['id']);
            $table->string(REF::DB_TABLES['pages']['fieldNames']['pagename'])->unique();
            $table->string(REF::DB_TABLES['pages']['fieldNames']['title']);
            $table->text(REF::DB_TABLES['pages']['fieldNames']['metadesc']);
            $table->text(REF::DB_TABLES['pages']['fieldNames']['keywords']);
        });
    }

    public function down() {
        Schema::dropIfExists(REF::DB_TABLES['pages']['tableName']);
    }
}