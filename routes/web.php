<?php

use App\Extenders\References as REF;

// http://mylanding-studio-admission-exam.loc/
Route::get('/', [   
    'as' =>      REF::OPEN_ROUTES['top'],
    'uses' =>   'Open\TopPageController@renderTopPage'
]);

// http://mylanding-studio-admission-exam.loc/no-js-request
Route::get('/no-js-request', [
    'as' => REF::OPEN_ROUTES['noJsRequest'],
    'uses' => 'Open\AuxiliaryPagesController@renderNoJsRequestPage'
]);

// Отправка заявки
Route::post('/submit-request', [
    'as' => REF::OPEN_ROUTES['submitConsultationRequest'],
    'uses' => 'Open\FeedbackController@submitConsultationRequest'
]);

// http://mylanding-studio-admission-exam.loc/thanks
Route::get('/thanks', [
    'as' => REF::OPEN_ROUTES['thanksForConsultationRequest'],
    'uses' => 'Open\AuxiliaryPagesController@renderThanksForConsultationRequestPage'
]);