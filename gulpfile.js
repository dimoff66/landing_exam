'use strict';

const   gulp = require('gulp'),
            
            plugins = require('gulp-load-plugins')(),
            plumber = require('gulp-plumber'),
            path = require('path'),
            rename = require('gulp-rename'),
            del = require('del'),
            
            // 其の他
            browserSync  = require('browser-sync'),
            cache       = require('gulp-cache');


let isDevelopment = true;
const HPath = require('./hikari/HPath').HPath;

function LazyRequireTask(taskName, path, options) {   
    
    options = options || {};
    
    options.taskName = taskName;
    options.HPath = HPath;
    
    gulp.task(taskName, function(callback){
        
        options.isDevelopment = isDevelopment;
        
        let task = require(path).call(this, options);
        return task(callback);
    });
}


// --- HTML 関連
LazyRequireTask('pug to html', HPath.gulpTasksPaths.pug2html);
LazyRequireTask('validate open html pages', HPath.gulpTasksPaths.validateOpenHtmlPages);
LazyRequireTask('prepare emails initial html', HPath.gulpTasksPaths.prepareEmailsInitialHtml);
LazyRequireTask('prepare emails final html', HPath.gulpTasksPaths.prepareEmailsFinalHtml);

// --- CSS 関連
LazyRequireTask('provide framework style dependences', HPath.gulpTasksPaths.provideFrameworkStyleDependences);
LazyRequireTask('styles', HPath.gulpTasksPaths.styles);

// --- Webpack 関連
LazyRequireTask('webpack', HPath.gulpTasksPaths.webpack);

// --- PHP 関連
LazyRequireTask('copy mock backend php files', HPath.gulpTasksPaths.copyMockBackendFiledToBuild);

// --- 画像関連
LazyRequireTask('images', HPath.gulpTasksPaths.images);

// --- 活字関連
LazyRequireTask('fonts', HPath.gulpTasksPaths.fonts);


gulp.task('clean previous development build', callback => {
    del.sync(HPath.developmentBuildAbsolutePath);
    callback();
});

gulp.task('clean previous production build', callback => {
    del.sync(HPath.laravelProductionBuildFilesSelectionWhichOkToRefresh);
    callback();
});


gulp.task('build', gulp.parallel(
        gulp.series('pug to html', 'validate open html pages', 'prepare emails initial html'), 
        gulp.series('provide framework style dependences', 'styles', 'prepare emails final html'),
        'webpack',
        'copy mock backend php files',
        'images', 'fonts',
        done => { done(); }
));

gulp.task('watch source files', () => {
    
    gulp.watch( HPath.pugSourceFilesSelection.watchedFiles, gulp.series('pug to html'));
    gulp.watch( HPath.emailPugSourceFilesSelection, gulp.series('prepare emails initial html', 'prepare emails final html'));
    gulp.watch( HPath.sassSourceFilesSelection.watchedFiles, gulp.series('styles'));
    gulp.watch( HPath.mockBackendPhpFilesSelection, gulp.series('copy mock backend php files'));
    gulp.watch( HPath.imagesSourceFilesSelection, gulp.series('images'));
});


gulp.task('static server', () => { 
    
    browserSync.init({
        server: HPath.devBuildStartPageRelPathBase
    });
    
    // 全ての更新が終わった後！
    browserSync.watch(HPath.allDevbuildFileSelector).on('change', browserSync.reload);
});

gulp.task('open server with browsersync', () => {
    
    browserSync.init({
        proxy: 'mylanding-studio.fed'
    });
    
    // 全ての更新が終わった後！
    browserSync.watch(HPath.allDevbuildFileSelector).on('change', browserSync.reload);
});


gulp.task('Development', gulp.series(

    done => { isDevelopment = true; done (); },
    'clean previous development build',
    'build',
    gulp.parallel('watch source files', 'static server')
    //gulp.parallel('watch source files', 'open server with browsersync')
));

gulp.task('default', gulp.series('Development'));


gulp.task('Production', gulp.series(
        
    done => { isDevelopment = false; done (); },
    'clean previous production build',
    'build'
));

gulp.task('clear cache', function () {
    return plugins.cache.clearAll();
});