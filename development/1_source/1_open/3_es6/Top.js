window.slider = new function () {
	
	let self = this;
	
	const SELECTED_CLASS 			    = 'selected';
	
	const TEXT_BLOCK_SELECTOR           = '.miniPictures .textBlock p';
	const MINIPICTURE_SELECTOR 	        = 'img.minipicture.' + SELECTED_CLASS;
	const SLIDER_SELECTOR_BYFILENAME 	= 'img.slider[filename="%"]';
	const SLIDER_SELECTOR			    = 'img.slider.selected'; 
	
	const SLIDE_INTERVAL       			= 10000;
	const SLIDE_INTERVAL_AUTO      		= 3000;
	
	const textBlock 	= document.querySelector(TEXT_BLOCK_SELECTOR);
	
	let modelProportion = 0;
	let timeoutId;
	
	this.onSelectMiniPicture = function(element, changedAutomaticly = true)
	{
            var newSelectionMini = element;
		
		var previousSelectionMini = document.querySelector(MINIPICTURE_SELECTOR);
		if(!previousSelectionMini) return;
		
		let [previousSelection, newSelection, ind] = getSlider(previousSelectionMini, newSelectionMini);
		
		if(!modelProportion){
			modelProportion = previousSelection.height / previousSelection.width;
		}	
		
		previousSelectionMini	.classList.remove(SELECTED_CLASS);
		previousSelection		.classList.remove(SELECTED_CLASS);
				
		textBlock.textContent = textBlock.dataset['sentence' + ind];	
		
		newSelectionMini	.classList.add(SELECTED_CLASS);
		newSelection		.classList.add(SELECTED_CLASS);
		
		setSliderHeight(newSelection);
		
		var interval = changedAutomaticly ? SLIDE_INTERVAL_AUTO : SLIDE_INTERVAL;
		
		if(!changedAutomaticly && timeoutId) clearTimeout(timeoutId);
		
		timeoutId = setTimeout(changeSlider, interval);
	}
	
	function getSlider(...minipictures)
	{
            let retVal = [];
            let ind = undefined;
        
            let slidersCol = document.querySelectorAll('img.slider');
            
            for(let minipicture of minipictures){
               ind = Array.from(minipicture.parentNode.children).indexOf(minipicture);
               retVal.push(slidersCol[ind]);
            }
        
            retVal.push(ind);
        
		return retVal	
	}
	
	function setSliderHeight(slider)
	{
		var height = Math.floor(slider.width * modelProportion);
		if(height !== slider.height){
			slider.height = height;		
		}
			
	}
	
    
    function changeSlider()
	{
		var currentSelection = document.querySelector(MINIPICTURE_SELECTOR);	
		var nextSlider = currentSelection.nextElementSibling;
		
		if(!nextSlider) nextSlider = currentSelection.parentNode.children[0]; 
		
		self.onSelectMiniPicture(nextSlider);
	}
	
	function onScreenResize()
	{
		/*var slider = document.querySelector(SLIDER_SELECTOR);
		setSliderHeight(slider);*/
	}
	
	timeoutId = setTimeout(changeSlider, SLIDE_INTERVAL_AUTO);
	
	//window.addEventListener('resize', onScreenResize);
}

window.modalDlg = new function () {
	
	var self = this;
	
	this.open = function(dlgSelector, 
								openedClassName = 'opened',
								modalBGSelector = '.modalBackground'){
																
		openClose(1, dlgSelector, openedClassName, modalBGSelector);
			
	}
	
	this.close = function(dlgSelector, 
								openedClassName = 'opened', 
								modalBGSelector = '.modalBackground'){
		
		openClose(0, dlgSelector, openedClassName, modalBGSelector);	
		
	}
	
	function openClose(open, dlgSelector, openedClassName, modalBGSelector)
	{
		var modalBackgroundElement = document.querySelector(modalBGSelector);
		if(!modalBackgroundElement){
			alert('Не найден элемент ' + modalBGSelector);
			return;
		}
		
		var modalWindowElement = modalBackgroundElement.querySelector(dlgSelector);
		if(!modalWindowElement){
			alert('Не найден элемент ' + dlgSelector);
			return;	
		}
		
		var display = open ? 'block' : 'none';
		
		modalBackgroundElement.style.display = display;
		
		if(open){
			modalWindowElement.className += ' ' + openedClassName;
			
			var closeFunction = function(){modalDlg.close(dlgSelector, openedClassName, modalBGSelector);}; 
			
			modalBackgroundElement.onclick = closeFunction;
			
			var closeButton = modalWindowElement.getElementsByClassName('closeDlgButton')[0];
			if(closeButton) closeButton.onclick = closeFunction;
			
			modalWindowElement.onclick = function(event){
				event.stopPropagation();
			}
			
		}
		else{
			modalWindowElement.classList.remove(openedClassName);		
		} 
		
		var overflowPropValue = open ? 'hidden' : 'inherit';
		document.body.style.overflowY = overflowPropValue;
			
	}	
}

window.cards = new function () {
	
    this.edit = function(target)
	{
        var infoCell = findCard(target).find('td.info');
        
		if(infoCell.is('.editMode')) return;
        
        infoCell.addClass('editMode').find('textarea')
            .text(infoCell.find('span').first().text())
            .focus();
	
	}
	
    this.delete = function(target)
    {
    	var card = findCard(target);
    	
        card.animate({opacity: '0'}, 'slow', 
                        
            function() {
                if($(card).addClass('deleted').parent().is('.column1')){
                    
                    let cardToMove = $(this).parent().next().find('.card').not('.deleted').first().detach();
                    
                    $(card).parent().append(cardToMove);
                }
                
            }
        );
        
        
    }
    
    this.finishEditing = function(infoInput)
    {
		var infoSpan = infoInput.previousElementSibling;
		infoSpan.textContent = infoInput.value;
		
		infoCell = infoInput.parentNode;
		infoCell.classList.remove('editMode');	
	}
    
    function findCard(elem)
    {
    	var card = $(elem).parentsUntil('table.card').last().parent();
		return card;	
	}
}

window.errors = new function () {
    
    this.check = function (event) {
        
        let testmode = true;
        
        let hasErrors = false;
        
        let fields = ['name', 'email', 'phone'];
        
        for(field of fields){
            
            let input = $('input[name="' + field + '"]');
            
            let errFlags = [!input.val(), input.val() && !input.get(0).validity.valid];
            
            if(input.hasClass('invalid') !== (errFlags[0] || errFlags[1]))
                input.toggleClass('invalid');
            
            let errorsDivs = $('.errorBlock.type_' + field).children();
            
            for(let i = 0; i < 2; i++){
                
                if(errorsDivs[i].classList.contains('show') !== errFlags[i]){
                    if(errFlags[i]) errorsDivs[i].classList.add('show'); 
                    else errorsDivs[i].classList.remove('show'); 
                }
            }
            
            hasErrors = hasErrors || errFlags[0] || errFlags[1];    
            
        }
        
        if($('.errorInfo').hasClass('show') != hasErrors) $('.errorInfo').toggleClass('show');
        
        if(hasErrors) event.preventDefault();    
        else if(testmode) alert('Форма успешно отправлена!!!');
        
    }
    
}