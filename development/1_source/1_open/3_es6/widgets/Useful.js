function modalDlgClass(){
	
	this.open = function(dlgSelector, openedClassName = 'opened',
								modalBGSelector = 'modalBackground'){
									
		openClose(1, dlgSelector, openedClassName, modalBGSelector);
			
	}
	
	this.close = function(dlgSelector, openedClassName = 'opened', modalBGSelector = 'modalBackground'){
		
		openClose(0, dlgSelector, openedClassName, modalBGSelector);	
		
	}
	
	function openClose(open, dlgSelector, openedClassName, modalBGSelector)
	{
		var modalBackgroundElement = document.querySelector(modalBGSelector);
		if(!modalBackgroundElement){
			alert('Не найден элемент ' + modalBGSelector);
			return;
		}
		
		var modalWindowElement = modalBackgroundElement.querySelector(dlgSelector);
		if(!modalWindowElement){
			alert('Не найден элемент ' + dlgSelector);
			return;	
		}
		
		var display = open ? 'block' : 'none';
		
		modalBackgroundElement.style.display = display;
		
		if(open)
			modalWindowElement.className += openedClassName;
		else 
			modalWindowElement.classList.remove(openedClassName);	
	}
	
}