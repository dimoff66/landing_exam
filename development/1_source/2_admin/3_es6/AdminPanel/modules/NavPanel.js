import MainContentManager from './MainContentManager';
let R;


export default class NavPanel {
        
    constructor(mainContentManager, Resources){
        
        this.mainContentManager = mainContentManager;
        R = Resources;
        
        this.$NavPanel = $(R.ccs.NavPanel.RootElement);
        this.navPanelClickableElementsCompoundSelector = R.createCommaSeparatedCompoundSelector(
                R.ccs.NavPanel.Item.RootElement,
                R.ccs.NavPanel.Item.ExpandButtonCell.RootElement,
                R.ccs.NavPanel.Subitem.Common);
                
        this.MENU_ITEM_IDS_AND_FRAGMENTS_COMPILANCE = {
                [R.id.NavPanel.LinkButtons.Statistics]: MainContentManager.FRAGMENTS.STATISTICS_TOP,
                [R.id.NavPanel.LinkButtons.Requests]: MainContentManager.FRAGMENTS.ALL_REQUESTS,
                [R.id.NavPanel.LinkButtons.RequestsSubitemDuplicate]: MainContentManager.FRAGMENTS.ALL_REQUESTS
        };
        
        this.initClickableElements();
    }
    
    
    initClickableElements(){
        
        this.$NavPanel.on('click', this.navPanelClickableElementsCompoundSelector, event => {
            
            event.stopPropagation();
            let $ClickedElement = $(event.currentTarget);
            
            if ($ClickedElement.hasClass(R.cc.NavPanel.Item.RootElement) ||
                    $ClickedElement.hasClass(R.cc.NavPanel.Subitem.Common)) {
                this.requestNewFragmentByNavPanelButtonId($ClickedElement.attr('id'));
            }
            else if ($ClickedElement.hasClass(R.cc.NavPanel.Item.ExpandButtonCell.RootElement)) {
                console.log('Expand');
            }
        });
    }
    
    requestNewFragmentByNavPanelButtonId(id){
        this.mainContentManager.renderAndInitializeNewFragment(this.MENU_ITEM_IDS_AND_FRAGMENTS_COMPILANCE[id]);
    }
}