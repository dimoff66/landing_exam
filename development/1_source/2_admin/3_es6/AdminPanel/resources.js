import Resources from  'FrameworkRoot/Resources';

export default class AdminPanelResources extends Resources {
        
    constructor() {
        
        const CSS_CLASSES = {
            
            Layout: {
                PageWrapper: 'AdminPanelLayout-PageWrapper',
                MainContent: 'AdminPanelLayout-MainContent'
            },
            
            NavPanel: {
                RootElement: 'AdminPanelLayout-NavPanel',
                WidthSetupModifiers: {
                   fixWidth: 'fixWidth',
                   fillParentWidth: 'fillParentWidth'
                },
                ItemsNode: 'NavPanel-ItemsNode',
                Item: {
                    RootElement: 'NavPanel-Item-Wrapper',
                    StatusModifiers: {
                        selected: 'selected',
                        disabled: 'disabled'
                    },
                    IconCell: 'NavPanel-Item-IconCell',
                    TitleCell: 'NavPanel-Item-TitleCell',
                    SpecialCell: 'NavPanel-Item-SpecialCell',
                    ExpandButtonCell: {
                        RootElement: 'NavPanel-Item-ExpandButtonCell',
                        Icons: {
                            expanded: 'expanded',
                            collapsed: 'collapsed'
                        }
                    }
                },
                Subitem: {
                    Common: 'NavPanel-Subitem',
                    SemanticModifiers: {
                        requestsSettings: 'requests-settings'
                    }
                }
            }
        };
        
        const ID = {
            NavPanel: {
                LinkButtons: {
                    Statistics: 'Statistics-NavMenuLinkButton',
                    Requests: 'Requests-NavMenuLinkButton',
                    RequestsSubitemDuplicate: 'RequestsSubitemDuplicate-NavMenuLinkButton',
                    RequestsSettings: 'RequestsSettings-NavMenuLinkButton',
                    Deals: 'Deals-NavMenuLinkButton',
                    SystemNotifications: 'SystemNotifications-NavMenuLinkButton'
                }
            }
        };
        
        const STRINGS = {
            ConsoleErrorMessages: {
                GettingFragmentHtmlFailure: 'Ошибка при получении данных с сервера'
            }
        };
        
        super(CSS_CLASSES, ID, STRINGS);
    }
}